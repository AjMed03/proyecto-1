/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.IOException;
import java.util.ArrayList;
import javax.mail.MessagingException;

/**
 *
 * @author Andrés Medina
 * @author Freddy Tenesaca
 * @author Aaron Macías
 */
public class Venta {
    private double precio;
    private ArrayList<Vehiculo> vehiculos;
    private ArrayList<Oferta> ofertas;
    private Rol rol;

    public Venta(double precio){
        this.precio=precio;
    }
    
    public double getPrecio(){
        return precio;
    }
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    public Rol getRol() {
        return rol;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    public static void aceptarOferta(String gmail, String tema, String desarrollo, String placa) throws MessagingException, IOException{
        Sistema.enviarConGMail(gmail, tema, desarrollo);
        Vehiculo.vehiculoVendido(placa);
        Oferta.ofertaConfirmada(placa);
        System.out.println("¡Correo enviado y Felicitacions por su venta!");
    }
    
    public static ArrayList<String> mostrarOfertas(String placa, ArrayList<String> ofertas){
        ArrayList<String> show = new ArrayList<>();
        StringBuilder s = new StringBuilder();
        for(String vh : ofertas){
            String placax = vh.split(">")[0];
            String[] xs = vh.split(">")[1].split(";");
            if(placax.equals(placa)){
                for(int i=0;i<xs.length;i++){
                s.append("Oferta ").append(i+1).append("\nCorreo: ").append(xs[i].split("-")[0]).append("\nPrecio Ofertado: ").append(xs[i].split("-")[1]);
                show.add(s.toString());
                }}}
        return show;
    }
    
    protected static ArrayList<String> criterioTipoUsuario(ArrayList<String> vehiculos,String tipo){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            String tipo_user = (v.split(";")[v.split(";").length-1]);
            if(tipo_user.equals(tipo)){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioPrecio(ArrayList<String> vehiculos,double precioInicial, double precioFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int precio = Integer.parseInt(v.split(";")[v.split(";").length-2]);
            if(precio>precioInicial&&precio<precioFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioAnual(ArrayList<String> vehiculos,int añoInicial, int añoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int año = Integer.parseInt(v.split(";")[v.split(";").length-4]);
            if(año>añoInicial&&año<añoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> criterioDistancia(ArrayList<String> vehiculos,int recorridoInicial, int recorridoFinal){
        ArrayList<String> mostrar = new ArrayList<>();
        for(String v : vehiculos){
            int recorrido = Integer.parseInt(v.split(";")[1]);
            if(recorrido>recorridoInicial&&recorrido<recorridoFinal){
                mostrar.add(v);
            }
        }
        return mostrar;
    }
    
    protected static ArrayList<String> validarCriterioAnual(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioAnual(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioAnual(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioDistancia(ArrayList<String> mostrar,ArrayList<String> vehiculos,int valorI, int valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioDistancia(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioDistancia(mostrar, valorI, valorF);
        return lista;
    }
    
    protected static ArrayList<String> validarCriterioPrecio(ArrayList<String> mostrar,ArrayList<String> vehiculos,double valorI, double valorF){
        ArrayList<String> lista = new ArrayList<>();
        if(contarLista(mostrar)==0)
            lista = Venta.criterioPrecio(vehiculos, valorI, valorF);
        else
            lista = Venta.criterioPrecio(mostrar, valorI, valorF);
        return lista;
    }

    public static int contarLista(ArrayList<String> mostrar){
        int contador = 0;
        for(String x : mostrar){
            ++contador;
        }
        return contador;
    }
    
    @Override
    public String toString(){
        return "Precio: "+this.precio;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Venta other = (Venta)obj;
        return this.precio==other.precio;
    }
}
